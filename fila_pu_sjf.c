#include "fila_pu_sjf.h"
#include "processo_sjf.h"
#include <stdio.h>
#include <stdlib.h>

void put(fila_pu_sjf *fila, processo_sjf *processo)
{
    if(fila->isEmpty(fila))
    {
        fila->header = processo;
        fila->numProcessos++;
    }
    else
    {
        processo_sjf *p = fila->header;
        
        if(fila->header->next == NULL)
        {
            if(processo->tempoExecucao > p->tempoExecucao)
            {
                p->next = processo;
                
                fila->numProcessos++;
            }
            else
            {
                processo->next = fila->header;
                fila->header = processo;
                
                fila->numProcessos++;
            }
        }
        else
        {
            if(processo->tempoExecucao < fila->header->tempoExecucao)
            {
                processo->next = fila->header;
                fila->header = processo;
                
                fila->numProcessos++;
                
                return;
            }
            
            while(p->next != NULL)
            {
                if(processo->tempoExecucao < p->next->tempoExecucao)
                {
                    processo_sjf *temp = p->next;
                    p->next = processo;
                    processo->next = temp;
                    
                    fila->numProcessos++;
                    
                    return;
                }
                else
                {
                    p = p->next;
                }
            }
            
            p->next = processo;
            
            fila->numProcessos++;
        }
    }
}

processo_sjf* get(fila_pu_sjf *fila)
{
    if(fila->isEmpty(fila))
    {
        return NULL;
    }
    else
    {
        if(fila->header->next != NULL)
        {
            processo_sjf *p = fila->header;
            fila->header = fila->header->next;
            
            fila->numProcessos--;
            
            return p;
        }
        else
        {
            processo_sjf *p = fila->header;
            fila->header = NULL;
            
            fila->numProcessos--;
            
            return p;
        }
    }
}

unsigned short isEmpty(fila_pu_sjf *fila)
{
    if(fila->header == NULL)
        return 1;
    else
        return 0;
}

fila_pu_sjf* criarFila()
{
    fila_pu_sjf *p_fila;
    p_fila = malloc(sizeof(fila_pu_sjf));
    
    p_fila->header = NULL;
    p_fila->get = get;
    p_fila->put = put;
    p_fila->isEmpty = isEmpty;
    
    return p_fila;
}