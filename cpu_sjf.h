#ifndef __CPU_SJF__
#define __CPU_SJF__

typedef struct escalonador_sjf escalonador_sjf;
typedef struct _GtkWidget GtkWidget;

void run_cpu(int numCores, escalonador_sjf* escalonador, GtkWidget* cores_box);

#endif