#ifndef __ESCALONADOR_SJF__
#define __ESCALONADOR_SJF__
typedef struct escalonador_sjf escalonador_sjf;
typedef struct fila_pu_sjf fila_pu_sjf;
typedef struct processo_sjf processo_sjf;

struct escalonador_sjf
{
    fila_pu_sjf* fila_pu;
    int numProcessos;
    
    void (*addProcessoEstatico)(fila_pu_sjf* fila, processo_sjf* processo);
    void (*addProcessoDinamico)(fila_pu_sjf* fila, processo_sjf* processo);
    processo_sjf* (*getProcesso)(fila_pu_sjf* fila);
};

void addProcessoEstatico(fila_pu_sjf* fila, processo_sjf* processo);

void addProcessoDinamico(fila_pu_sjf* fila, processo_sjf* processo);

processo_sjf* getProcesso(fila_pu_sjf* fila);

escalonador_sjf* criarEscalonador(int numProcessos);
#endif