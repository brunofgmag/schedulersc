#ifndef __FRAME_SJF__
#define __FRAME_SJF__

typedef struct _GtkWidget GtkWidget;
struct escalonador_sjf *escalonador;
unsigned int index_processo;

typedef struct _ButtonParams
{
    GtkWidget *field_cores;
    GtkWidget *field_processos;
    GtkWidget *button;
    GtkWidget *button_add;
    GtkWidget *box_cores;
    GtkWidget *box_processos;
    GtkWidget *box_finalizados;
} ButtonParams;

void init_frame_sjf(GtkWidget *notebook);

#endif