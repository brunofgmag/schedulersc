#include "escalonador_sjf.h"
#include "fila_pu_sjf.h"
#include <stdlib.h>

void addProcessoEstatico(fila_pu_sjf* fila, processo_sjf* processo)
{
    fila->put(fila, processo);
}

void addProcessoDinamico(fila_pu_sjf* fila, processo_sjf* processo)
{
    fila->put(fila, processo);
}

processo_sjf* getProcesso(fila_pu_sjf* fila)
{
    return fila->get(fila);
}

escalonador_sjf* criarEscalonador(int numProcessos)
{
    escalonador_sjf *p;
    p = malloc(sizeof(escalonador_sjf));
    
    p->fila_pu = criarFila();
    p->getProcesso = getProcesso;
    p->addProcessoEstatico = addProcessoEstatico;
    p->addProcessoDinamico = addProcessoDinamico;
    p->numProcessos = numProcessos;
    
    return p;
}