.PHONY: clean All

All:
	@echo "----------Building project:[ EscalonadoresC - Debug ]----------"
	@"$(MAKE)" -f  "EscalonadoresC.mk"
clean:
	@echo "----------Cleaning project:[ EscalonadoresC - Debug ]----------"
	@"$(MAKE)" -f  "EscalonadoresC.mk" clean
