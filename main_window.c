#include "main_window.h"
#include "frame_sjf.h"
#include <stdio.h>
#include <gtk/gtk.h>

GtkWidget* getWindow()
{
    struct _GtkWidget GtkWidget, *p;
    p = &GtkWidget;
    
    return p;
}

void init_main_window(int argc, char* argv[])
{
    GtkWidget *window = getWindow();
    GtkWidget *notebook;
    GtkWidget *grid;
    GtkCssProvider *css_provider;
    
    //Torna o GTK thread-aware
    gdk_threads_init();
    
    //Inicia o GTK
    gtk_init (&argc, &argv);
    
    //Cria uma nova janela
    window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
    
    //Carrega o arquivo CSS
    css_provider = gtk_css_provider_new();
    gtk_css_provider_load_from_path(css_provider, "default.css", NULL);
    gtk_style_context_add_provider_for_screen(gdk_screen_get_default(), GTK_STYLE_PROVIDER(css_provider), GTK_STYLE_PROVIDER_PRIORITY_USER);
    
    //Termina a aplicação quando fechar a janela
    g_signal_connect(G_OBJECT(window), "destroy", G_CALLBACK(gtk_main_quit), NULL);
    
    grid = gtk_grid_new();
    
    /* Create a new notebook, place the position of the tabs */
    notebook = gtk_notebook_new ();
    gtk_notebook_set_tab_pos (GTK_NOTEBOOK (notebook), GTK_POS_TOP);
    gtk_grid_attach(GTK_GRID (grid), notebook, 1, 1, 1, 1);
    gtk_container_add (GTK_CONTAINER (window), grid);
    
    gtk_widget_show (notebook);
    
    init_frame_sjf(notebook);
    
    gtk_widget_show_all(window);
    
    gtk_main ();
}