#include "core_sjf.h"
#include "escalonador_sjf.h"
#include "estados.h"
#include "processo_sjf.h"
#include "tipos_processo.h"
#include <pthread.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <gtk/gtk.h>

void gui_func_init_core(void* arg)
{
    core_sjf* core = arg;
    GValue editable_value = G_VALUE_INIT;
    GtkStyleContext* style_context;
    
    style_context = gtk_widget_get_style_context(core->text_area);
    core->buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(core->text_area));
    g_value_init(&editable_value, G_TYPE_BOOLEAN);
    g_value_set_boolean(&editable_value, FALSE);
    gtk_style_context_add_class(style_context, "white-background");
    gtk_text_view_set_editable(GTK_TEXT_VIEW(core->text_area), FALSE);
    g_object_set_property(G_OBJECT(core->text_area), "can_focus", &editable_value);
    
    gtk_box_pack_start(GTK_BOX(core->cores_box), core->text_area, FALSE, FALSE, 0);
    
    gtk_widget_show(core->text_area);
    
    g_value_unset(&editable_value);
}

void gui_func_change_text_core(void* arg)
{
    core_sjf* core = arg;
    GtkStyleContext* style_context;
    
    style_context = gtk_widget_get_style_context(core->text_area);
    
    gtk_style_context_remove_class(style_context, "white-background");
    gtk_style_context_remove_class(style_context, "gray-background");
    gtk_style_context_remove_class(style_context, "red-background");
    
    if(core->processo != NULL)
    {
        if(core->processo->tipo_processo == ESTATICO)
        {
            gtk_style_context_add_class(style_context, "gray-background");
        }
        else
        {
            gtk_style_context_add_class(style_context, "red-background");
        }
    }
    else
    {
        gtk_style_context_add_class(style_context, "white-background");
    }
    
    gtk_text_buffer_set_text(core->buffer, core->text, -1);
}

void thread_core_sjf(void* arg)
{
    core_sjf *core = arg;

    g_idle_add((GSourceFunc)gui_func_init_core, core);

    while(1)
    {
        if(core->processo == NULL || core->processo->estadoProcesso == FINALIZADO)
        {
            core->processo = core->escalonador->getProcesso(core->escalonador->fila_pu);
        }
        
        if(core->processo != NULL)
        {
            if(core->processo->estadoProcesso == EXECUTANDO || core->processo->estadoProcesso == PRONTO)
            {
                core->processo->estadoProcesso = EXECUTANDO;
                core->text = g_strdup_printf (" ID: %d             \n TE: %d             \n TR: %d                \n                     ", core->processo->idProcesso, core->processo->tempoExecucao, core->processo->tempoRestante);
                g_idle_add((GSourceFunc)gui_func_change_text_core, core);
            }
            else
            {
                core->processo = NULL;
                core->text = "                  \n                  \n                     \n                     ";
                g_idle_add((GSourceFunc)gui_func_change_text_core, core);
            }
        }
        else
        {
            core->processo = NULL;
            core->text = "                  \n                  \n                     \n                     ";
            g_idle_add((GSourceFunc)gui_func_change_text_core, core);
        }
        usleep(300000);
    }
}

void run_core_sjf(core_sjf *core)
{
    int err = 0;
    pthread_t tid;
    
    err = pthread_create(&tid, NULL, (void*)thread_core_sjf, core);
    
    if (err != 0)
        printf("\nErro ao criar thread de core: [%s]", strerror(err));
}

core_sjf* criarCore(int numCore, escalonador_sjf* escalonador, GtkWidget* box)
{
    core_sjf *p_core;
    p_core = malloc(sizeof(core_sjf));
    
    p_core->numCore = numCore;
    p_core->run = run_core_sjf;
    p_core->escalonador = escalonador;
    p_core->cores_box = box;
    p_core->text_area = gtk_text_view_new();
    p_core->processo = NULL;
    p_core->buffer = NULL;
    
    p_core->run(p_core);
    
    return p_core;
}