#ifndef __CORE_SJF__
#define __CORE_SJF__

typedef struct processo_sjf processo_sjf;
typedef struct core_sjf core_sjf;
typedef struct escalonador_sjf escalonador_sjf;
typedef struct _GtkWidget GtkWidget;
typedef struct _GtkTextBuffer GtkTextBuffer;
typedef char gchar;

struct core_sjf 
{
    unsigned short numCore;
    escalonador_sjf* escalonador;
    processo_sjf* processo;
    GtkWidget* text_area;
    GtkWidget* cores_box;
    GtkTextBuffer *buffer;
    gchar* text;
    
    void (*run)(core_sjf *processo);
};

core_sjf* criarCore();

#endif