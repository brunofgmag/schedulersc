##
## Auto Generated makefile by CodeLite IDE
## any manual changes will be erased      
##
## Debug
ProjectName            :=EscalonadoresC
ConfigurationName      :=Debug
WorkspacePath          :=/home/bruno/Documentos/ProjetosC
ProjectPath            :=/home/bruno/Documentos/ProjetosC
IntermediateDirectory  :=./Debug
OutDir                 := $(IntermediateDirectory)
CurrentFileName        :=
CurrentFilePath        :=
CurrentFileFullPath    :=
User                   :=Bruno Magalhães
Date                   :=17/10/17
CodeLitePath           :=/home/bruno/.codelite
LinkerName             :=/usr/bin/g++
SharedObjectLinkerName :=/usr/bin/g++ -shared -fPIC
ObjectSuffix           :=.o
DependSuffix           :=.o.d
PreprocessSuffix       :=.i
DebugSwitch            :=-g 
IncludeSwitch          :=-I
LibrarySwitch          :=-l
OutputSwitch           :=-o 
LibraryPathSwitch      :=-L
PreprocessorSwitch     :=-D
SourceSwitch           :=-c 
OutputFile             :=$(IntermediateDirectory)/$(ProjectName)
Preprocessors          :=
ObjectSwitch           :=-o 
ArchiveOutputSwitch    := 
PreprocessOnlySwitch   :=-E
ObjectsFileList        :="EscalonadoresC.txt"
PCHCompileFlags        :=
MakeDirCommand         :=mkdir -p
LinkOptions            :=  $(shell pkg-config --libs gtk+-3.0) -pthread 
IncludePath            :=  $(IncludeSwitch). $(IncludeSwitch). 
IncludePCH             := 
RcIncludePath          := 
Libs                   := 
ArLibs                 :=  
LibPath                := $(LibraryPathSwitch). 

##
## Common variables
## AR, CXX, CC, AS, CXXFLAGS and CFLAGS can be overriden using an environment variables
##
AR       := /usr/bin/ar rcu
CXX      := /usr/bin/g++
CC       := /usr/bin/gcc
CXXFLAGS :=  -g -O0 -Wall $(Preprocessors)
CFLAGS   :=  -g -Wall -O0 $(shell pkg-config --cflags gtk+-3.0) $(Preprocessors)
ASFLAGS  := 
AS       := /usr/bin/as


##
## User defined environment variables
##
CodeLiteDir:=/usr/share/codelite
Objects0=$(IntermediateDirectory)/main.c$(ObjectSuffix) $(IntermediateDirectory)/core_sjf.c$(ObjectSuffix) $(IntermediateDirectory)/fila_pu_sjf.c$(ObjectSuffix) $(IntermediateDirectory)/main_window.c$(ObjectSuffix) $(IntermediateDirectory)/processo_sjf.c$(ObjectSuffix) $(IntermediateDirectory)/frame_sjf.c$(ObjectSuffix) $(IntermediateDirectory)/escalonador_sjf.c$(ObjectSuffix) $(IntermediateDirectory)/cpu_sjf.c$(ObjectSuffix) 



Objects=$(Objects0) 

##
## Main Build Targets 
##
.PHONY: all clean PreBuild PrePreBuild PostBuild MakeIntermediateDirs
all: $(OutputFile)

$(OutputFile): $(IntermediateDirectory)/.d $(Objects) 
	@$(MakeDirCommand) $(@D)
	@echo "" > $(IntermediateDirectory)/.d
	@echo $(Objects0)  > $(ObjectsFileList)
	$(LinkerName) $(OutputSwitch)$(OutputFile) @$(ObjectsFileList) $(LibPath) $(Libs) $(LinkOptions)

MakeIntermediateDirs:
	@test -d ./Debug || $(MakeDirCommand) ./Debug


$(IntermediateDirectory)/.d:
	@test -d ./Debug || $(MakeDirCommand) ./Debug

PreBuild:


##
## Objects
##
$(IntermediateDirectory)/main.c$(ObjectSuffix): main.c $(IntermediateDirectory)/main.c$(DependSuffix)
	$(CC) $(SourceSwitch) "/home/bruno/Documentos/ProjetosC/main.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/main.c$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/main.c$(DependSuffix): main.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/main.c$(ObjectSuffix) -MF$(IntermediateDirectory)/main.c$(DependSuffix) -MM main.c

$(IntermediateDirectory)/main.c$(PreprocessSuffix): main.c
	$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/main.c$(PreprocessSuffix) main.c

$(IntermediateDirectory)/core_sjf.c$(ObjectSuffix): core_sjf.c $(IntermediateDirectory)/core_sjf.c$(DependSuffix)
	$(CC) $(SourceSwitch) "/home/bruno/Documentos/ProjetosC/core_sjf.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/core_sjf.c$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/core_sjf.c$(DependSuffix): core_sjf.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/core_sjf.c$(ObjectSuffix) -MF$(IntermediateDirectory)/core_sjf.c$(DependSuffix) -MM core_sjf.c

$(IntermediateDirectory)/core_sjf.c$(PreprocessSuffix): core_sjf.c
	$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/core_sjf.c$(PreprocessSuffix) core_sjf.c

$(IntermediateDirectory)/fila_pu_sjf.c$(ObjectSuffix): fila_pu_sjf.c $(IntermediateDirectory)/fila_pu_sjf.c$(DependSuffix)
	$(CC) $(SourceSwitch) "/home/bruno/Documentos/ProjetosC/fila_pu_sjf.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/fila_pu_sjf.c$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/fila_pu_sjf.c$(DependSuffix): fila_pu_sjf.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/fila_pu_sjf.c$(ObjectSuffix) -MF$(IntermediateDirectory)/fila_pu_sjf.c$(DependSuffix) -MM fila_pu_sjf.c

$(IntermediateDirectory)/fila_pu_sjf.c$(PreprocessSuffix): fila_pu_sjf.c
	$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/fila_pu_sjf.c$(PreprocessSuffix) fila_pu_sjf.c

$(IntermediateDirectory)/main_window.c$(ObjectSuffix): main_window.c $(IntermediateDirectory)/main_window.c$(DependSuffix)
	$(CC) $(SourceSwitch) "/home/bruno/Documentos/ProjetosC/main_window.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/main_window.c$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/main_window.c$(DependSuffix): main_window.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/main_window.c$(ObjectSuffix) -MF$(IntermediateDirectory)/main_window.c$(DependSuffix) -MM main_window.c

$(IntermediateDirectory)/main_window.c$(PreprocessSuffix): main_window.c
	$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/main_window.c$(PreprocessSuffix) main_window.c

$(IntermediateDirectory)/processo_sjf.c$(ObjectSuffix): processo_sjf.c $(IntermediateDirectory)/processo_sjf.c$(DependSuffix)
	$(CC) $(SourceSwitch) "/home/bruno/Documentos/ProjetosC/processo_sjf.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/processo_sjf.c$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/processo_sjf.c$(DependSuffix): processo_sjf.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/processo_sjf.c$(ObjectSuffix) -MF$(IntermediateDirectory)/processo_sjf.c$(DependSuffix) -MM processo_sjf.c

$(IntermediateDirectory)/processo_sjf.c$(PreprocessSuffix): processo_sjf.c
	$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/processo_sjf.c$(PreprocessSuffix) processo_sjf.c

$(IntermediateDirectory)/frame_sjf.c$(ObjectSuffix): frame_sjf.c $(IntermediateDirectory)/frame_sjf.c$(DependSuffix)
	$(CC) $(SourceSwitch) "/home/bruno/Documentos/ProjetosC/frame_sjf.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/frame_sjf.c$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/frame_sjf.c$(DependSuffix): frame_sjf.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/frame_sjf.c$(ObjectSuffix) -MF$(IntermediateDirectory)/frame_sjf.c$(DependSuffix) -MM frame_sjf.c

$(IntermediateDirectory)/frame_sjf.c$(PreprocessSuffix): frame_sjf.c
	$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/frame_sjf.c$(PreprocessSuffix) frame_sjf.c

$(IntermediateDirectory)/escalonador_sjf.c$(ObjectSuffix): escalonador_sjf.c $(IntermediateDirectory)/escalonador_sjf.c$(DependSuffix)
	$(CC) $(SourceSwitch) "/home/bruno/Documentos/ProjetosC/escalonador_sjf.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/escalonador_sjf.c$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/escalonador_sjf.c$(DependSuffix): escalonador_sjf.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/escalonador_sjf.c$(ObjectSuffix) -MF$(IntermediateDirectory)/escalonador_sjf.c$(DependSuffix) -MM escalonador_sjf.c

$(IntermediateDirectory)/escalonador_sjf.c$(PreprocessSuffix): escalonador_sjf.c
	$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/escalonador_sjf.c$(PreprocessSuffix) escalonador_sjf.c

$(IntermediateDirectory)/cpu_sjf.c$(ObjectSuffix): cpu_sjf.c $(IntermediateDirectory)/cpu_sjf.c$(DependSuffix)
	$(CC) $(SourceSwitch) "/home/bruno/Documentos/ProjetosC/cpu_sjf.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/cpu_sjf.c$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/cpu_sjf.c$(DependSuffix): cpu_sjf.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/cpu_sjf.c$(ObjectSuffix) -MF$(IntermediateDirectory)/cpu_sjf.c$(DependSuffix) -MM cpu_sjf.c

$(IntermediateDirectory)/cpu_sjf.c$(PreprocessSuffix): cpu_sjf.c
	$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/cpu_sjf.c$(PreprocessSuffix) cpu_sjf.c


-include $(IntermediateDirectory)/*$(DependSuffix)
##
## Clean
##
clean:
	$(RM) -r ./Debug/


