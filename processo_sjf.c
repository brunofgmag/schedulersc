#include "processo_sjf.h"
#include "estados.h"
#include "tipos_processo.h"
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <pthread.h>
#include <unistd.h>
#include <string.h>
#include <gtk/gtk.h>

void gui_func_init(void* arg)
{
    processo_sjf* processo = arg;
    GValue editable_value = G_VALUE_INIT;
    GtkStyleContext* style_context;
    
    style_context = gtk_widget_get_style_context(processo->text_area);
    processo->buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(processo->text_area));
    g_value_init(&editable_value, G_TYPE_BOOLEAN);
    g_value_set_boolean(&editable_value, FALSE);
    gtk_style_context_add_class(style_context, "white-background");
    gtk_text_view_set_editable(GTK_TEXT_VIEW(processo->text_area), FALSE);
    g_object_set_property(G_OBJECT(processo->text_area), "can_focus", &editable_value);
    
    processo->text = g_strdup_printf (" ID: %d             \n TE: %d             \n TR: %d                \n                     ", processo->idProcesso, processo->tempoExecucao, processo->tempoRestante);
    gtk_text_buffer_set_text(processo->buffer, processo->text, -1);
    
    gtk_box_pack_end(GTK_BOX(processo->processos_box), processo->text_area, FALSE, FALSE, 0);
    
    gtk_widget_show(processo->text_area);
    
    if(processo->is_last)
    {
        sort(processo->processos_box);
    }
    
    g_value_unset(&editable_value);
}

void gui_func_change_text(void* arg)
{
    processo_sjf* processo = arg;
    GtkStyleContext* style_context;
    
    style_context = gtk_widget_get_style_context(processo->text_area);
    
    gtk_style_context_remove_class    (style_context, "white-background");
    gtk_style_context_remove_class(style_context, "gray-background");
    gtk_style_context_remove_class(style_context, "red-background");
    
    if(processo->tipo_processo == ESTATICO)
    {
        gtk_style_context_add_class(style_context, "gray-background");
    }
    else
    {
        gtk_style_context_add_class(style_context, "red-background");
    }
    
    gtk_text_buffer_set_text(processo->buffer, processo->text, -1);
}

void gui_func_finalizar(void* arg)
{
    processo_sjf* processo = arg;
    
    gtk_box_pack_start(GTK_BOX(processo->finalizados_box), processo->text_area, FALSE, FALSE, 0);
}

void gui_func_executar(void* arg)
{
    processo_sjf* processo = arg;
    
    g_object_ref(processo->text_area);
    gtk_container_remove(GTK_CONTAINER(processo->processos_box), processo->text_area);
}

static gint compare(gconstpointer a, gconstpointer b)
{
    GtkTextBuffer* buff_a;
    GtkTextBuffer* buff_b;
    long te_1;
    long te_2;
    gchar* str1;
    gchar* str2;
    char str_value1[2];
    char str_value2[2];
    GtkTextIter start_1;
    GtkTextIter end_1;
    GtkTextIter start_2;
    GtkTextIter end_2;
    int i = 0;
    
    buff_a = gtk_text_view_get_buffer(GTK_TEXT_VIEW(a));
    buff_b = gtk_text_view_get_buffer(GTK_TEXT_VIEW(b));
    
    gtk_text_buffer_get_start_iter(buff_a, &start_1);
    gtk_text_buffer_get_start_iter(buff_b, &start_2);
    
    gtk_text_buffer_get_end_iter(buff_a, &end_1);
    gtk_text_buffer_get_end_iter(buff_b, &end_2);
    
    str1 = gtk_text_buffer_get_text(buff_a, &start_1, &end_1, FALSE);
    str2 = gtk_text_buffer_get_text(buff_b, &start_2, &end_2, FALSE);
    
    while(1)
    {
        if(str1[0] == 'T' && str1[1] == 'E')
        {
            i++;
        }
        
        if(i >= 1)
        {
            i++;
        }
        
        str1++;
        
        if(i == 5)
        {
            break;
        }
    }
    
    i = 0;
    
    while(1)
    {
        if(str2[0] == 'T' && str2[1] == 'E')
        {
            i++;
        }
        
        if(i >= 1)
        {
            i++;
        }
        
        str2++;
        
        if(i == 5)
        {
            break;
        }
    }
    
    str_value1[0] = str1[0];
    str_value1[1] = str1[1];
    
    str_value2[0] = str2[0];
    str_value2[1] = str2[1];
    
    te_1 = strtol(str_value1, (char**)NULL, 10);
    te_2 = strtol(str_value2, (char**)NULL, 10);
    
    if(te_1 < te_2)
    {
        return -1;
    }
    else if(te_1 > te_2)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

void sort(void* box_container)
{
    GtkWidget* box = box_container;
    GList* childrens;
    GList* iter;
    
    childrens = gtk_container_get_children(GTK_CONTAINER(box));
    
    childrens = g_list_sort(childrens, compare);
    
    for(iter = childrens; iter != NULL; iter = g_list_next(iter))
    {
        g_object_ref(GTK_WIDGET(iter->data));
        gtk_container_remove(GTK_CONTAINER(box), GTK_WIDGET(iter->data));
    }
    
    for(; childrens != NULL; childrens = g_list_next(childrens))
    {
        gtk_box_pack_start(GTK_BOX(box), GTK_WIDGET(childrens->data), FALSE, FALSE, 0);
    }
    
    g_list_free(childrens);
    g_list_free(iter);
}

void thread_processo_sjf(void *arg)
{
    processo_sjf *processo = arg;
    
    g_idle_add((GSourceFunc)gui_func_init, processo);
    
    processo->text = g_strdup_printf (" ID: %d             \n TE: %d             \n TR: %d                \n                     ", processo->idProcesso, processo->tempoExecucao, processo->tempoRestante);
    
    g_idle_add((GSourceFunc)gui_func_change_text, processo);
    
    while(processo->estadoProcesso != FINALIZADO)
    {
        if(processo->estadoProcesso == EXECUTANDO)
        {
            g_idle_add((GSourceFunc)gui_func_executar, processo);
            sleep(1);
            if(processo->tempoRestante > 0)
            {
                processo->tempoRestante--;
                
                if(processo->tempoRestante == 0)
                {
                    g_idle_add((GSourceFunc)gui_func_finalizar, processo);
                    processo->estadoProcesso = FINALIZADO;
                    pthread_exit(0);
                }
            }
        }
        else
        {
            usleep(500000);
        }
    }
}

void run_processo_sjf(processo_sjf *processo)
{
    int tempo = random_number(4, 20, processo->idProcesso);
    int err = 0;
    pthread_t tid;
    
    processo->tempoExecucao = tempo;
    processo->tempoRestante = tempo;
    
    err = pthread_create(&tid, NULL, (void*)thread_processo_sjf, processo);
    
    if (err != 0)
        printf("\nErro ao criar thread: [%s]", strerror(err));
}

processo_sjf* criarProcesso(int idProcesso, int tipo_processo, GtkWidget* box, GtkWidget* fin_box, unsigned short is_last)
{
    processo_sjf *p_processo;
    p_processo = malloc(sizeof(processo_sjf));
    
    p_processo->idProcesso = idProcesso;
    p_processo->estadoProcesso = PRONTO;
    p_processo->run = run_processo_sjf;
    p_processo->tipo_processo = tipo_processo;
    p_processo->text_area = gtk_text_view_new();
    p_processo->processos_box = box;
    p_processo->finalizados_box = fin_box;
    p_processo->is_last = is_last;
    p_processo->next = NULL;
    
    p_processo->run(p_processo);
    
    return p_processo;
}

int random_number(int min_num, int max_num, int idProcesso)
{
    int result = 0, low_num = 0, hi_num = 0;

    if (min_num < max_num)
    {
        low_num = min_num;
        hi_num = max_num + 1;
    } else {
        low_num = max_num + 1;
        hi_num = min_num;
    }

    srand(time(NULL) + idProcesso);
    result = (rand() % (hi_num - low_num)) + low_num;
    return result;
}