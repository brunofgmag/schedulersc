#ifndef __FILA_PU_SJF__
#define __FILA_PU_SJF__

typedef struct fila_pu_sjf fila_pu_sjf;
typedef struct processo_sjf processo_sjf;

struct fila_pu_sjf
{
    unsigned int numProcessos;
    processo_sjf* header;
    
    void (*put)(fila_pu_sjf* fila, processo_sjf* processo);
    processo_sjf* (*get)(fila_pu_sjf* fila);
    unsigned short (*isEmpty)(fila_pu_sjf* fila);
};

void put(fila_pu_sjf* fila, processo_sjf* processo);

processo_sjf* get(fila_pu_sjf* fila);

unsigned short isEmpty(fila_pu_sjf* Sfila);

fila_pu_sjf* criarFila();

#endif