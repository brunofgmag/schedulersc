#include "frame_sjf.h"
#include "processo_sjf.h"
#include "cpu_sjf.h"
#include "escalonador_sjf.h"
#include "fila_pu_sjf.h"
#include "tipos_processo.h"
#include <gtk/gtk.h>
#include <ctype.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>

static void scale_moved(GtkRange *range, gpointer user_data)
{
    GtkWidget *field_cores = user_data;
    
    gchar *str = g_strdup_printf ("%.0f", gtk_range_get_value(range));
    
    gtk_entry_set_text(GTK_ENTRY(field_cores), str);
    
    g_free(str);
}

static void entry_number_filter (GtkEntry *entry, const gchar *text, gint length, gint *position, gpointer data)
{
  GtkEditable *editable = GTK_EDITABLE(entry);
  int i;
  int count = 0;
  gchar *result = g_new (gchar, length);

  for (i = 0; i < length; i++) {
    if (!isdigit(text[i]))
      continue;
    result[count++] = text[i];
  }
  
  if (count > 0) {
    g_signal_handlers_block_by_func(G_OBJECT (editable), G_CALLBACK (entry_number_filter), data);
    gtk_editable_insert_text(editable, result, count, position);
    g_signal_handlers_unblock_by_func(G_OBJECT (editable), G_CALLBACK (entry_number_filter), data);
  }
  g_signal_stop_emission_by_name(G_OBJECT (editable), "insert-text");

  g_free(result);
}

void button_clicked_add(GtkButton *button, gpointer user_data)
{
    ButtonParams *p_ButtonParams = user_data;
    escalonador->addProcessoEstatico(escalonador->fila_pu, criarProcesso(index_processo++, DINAMICO, p_ButtonParams->box_processos, p_ButtonParams->box_finalizados, 1));
}

void button_clicked(GtkButton *button, gpointer user_data)
{
    ButtonParams *p_ButtonParams = user_data;
    gint16 processos_length = gtk_entry_get_text_length(GTK_ENTRY(p_ButtonParams->field_processos));
    gint16 cores_length = gtk_entry_get_text_length(GTK_ENTRY(p_ButtonParams->field_cores));
    gchar *processos_string = g_new(gchar, processos_length);
    gchar *cores_string = g_new(gchar, cores_length);
    int num_processos = atoi(gtk_entry_get_text(GTK_ENTRY(p_ButtonParams->field_processos)));
    int num_cores = atoi(gtk_entry_get_text(GTK_ENTRY(p_ButtonParams->field_cores)));
    escalonador = criarEscalonador(num_processos);
    fila_pu_sjf *fila = escalonador->fila_pu;
    processo_sjf* processo;
    
    if(processos_length == 0)
    {
        num_processos = 1;
        gtk_entry_set_text(GTK_ENTRY(p_ButtonParams->field_processos), "1");
    }
    
    gtk_widget_set_sensitive(p_ButtonParams->button, FALSE);
    gtk_widget_set_sensitive(p_ButtonParams->button_add, TRUE);
    
    for(index_processo = 0; index_processo < num_processos; index_processo++)
    {
        unsigned short is_last = 0;
        
        if(index_processo == (num_processos - 1))
        {
            is_last = 1;
        }
        
        processo = criarProcesso(index_processo, ESTATICO, p_ButtonParams->box_processos, p_ButtonParams->box_finalizados, is_last);
        escalonador->addProcessoEstatico(fila, processo);
        printf("Criando processo estatico %d\n", index_processo);
    }
    
    run_cpu(num_cores, escalonador, p_ButtonParams->box_cores);
    
    g_free(processos_string);
    g_free(cores_string);
}


void init_frame_sjf(GtkWidget *notebook)
{
    GtkWidget *frame;
    GtkWidget *label_sjf;
    GtkWidget *label_cores;
    GtkWidget *field_cores;
    GtkWidget *num_cores;
    GtkWidget *button_add_dinamic;
    GtkWidget *label_processos;
    GtkWidget *field_processos;
    GtkWidget *button_iniciar;
    GtkWidget *init_box;
    GtkWidget *cores_box;
    GtkWidget *processos_box;
    GtkWidget *button_box;
    GtkWidget *main_box;
    GtkWidget *container_cores_box;
    GtkWidget *cores_running_label;
    GtkWidget *cores_running_box;
    GtkWidget *container_processos_box;
    GtkWidget *container_processos_label;
    GtkWidget *processos_fila_box;
    GtkWidget *container_finalizados_box;
    GtkWidget *finalizados_label;
    GtkWidget *finalizados_box;
    GtkWidget *scrolled_window;

    GValue editable_value = G_VALUE_INIT;
    
    ButtonParams *p_ButtonParams;
    p_ButtonParams = malloc(sizeof(ButtonParams));
    
    g_value_init(&editable_value, G_TYPE_BOOLEAN);
    
    //criando as boxes
    scrolled_window = gtk_scrolled_window_new(NULL, NULL);
    main_box = gtk_box_new(GTK_ORIENTATION_VERTICAL, 10);
    init_box = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 10);
    cores_box = gtk_box_new(GTK_ORIENTATION_VERTICAL, 5);
    processos_box = gtk_box_new(GTK_ORIENTATION_VERTICAL, 5);
    button_box = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
    container_cores_box = gtk_box_new(GTK_ORIENTATION_VERTICAL, 10);
    cores_running_box = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 10);
    container_processos_box = gtk_box_new(GTK_ORIENTATION_VERTICAL, 10);
    processos_fila_box = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 10);
    container_finalizados_box = gtk_box_new(GTK_ORIENTATION_VERTICAL, 10);
    finalizados_box = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 10);
    
    
    //criando o frame principal
    frame = gtk_frame_new("Escalonador SJF");
    gtk_container_set_border_width(GTK_CONTAINER (frame), 5);
    gtk_widget_set_size_request(frame, 500, 500);
    gtk_widget_set_hexpand(frame, TRUE);
    gtk_widget_set_vexpand(frame, TRUE);
    
    //configurando box de cores
    label_cores = gtk_label_new("Número de cores:");
    gtk_widget_set_valign(label_cores, GTK_ALIGN_START);
    gtk_widget_set_halign(label_cores, GTK_ALIGN_START);
    gtk_widget_set_margin_top(label_cores, 5);
    gtk_widget_set_margin_start(label_cores, 5);
    
    field_cores = gtk_entry_new();
    g_value_set_boolean(&editable_value, FALSE);
    g_object_set_property(G_OBJECT(field_cores), "editable", &editable_value);
    g_object_set_property(G_OBJECT(field_cores), "can_focus", &editable_value);
    gtk_entry_set_max_length(GTK_ENTRY(field_cores), 2);
    gtk_widget_set_valign(field_cores, GTK_ALIGN_START);
    gtk_widget_set_margin_start(field_cores, 5);
    gtk_entry_set_text(GTK_ENTRY(field_cores), "1");
    
    num_cores = gtk_scale_new_with_range(GTK_ORIENTATION_HORIZONTAL, 1, 64, 1);
    gtk_scale_set_draw_value(GTK_SCALE(num_cores), FALSE);
    g_signal_connect(G_OBJECT(num_cores), "value-changed", G_CALLBACK(scale_moved), field_cores);
    gtk_widget_set_margin_start(num_cores, 5);
    
    gtk_widget_set_size_request(cores_box, 10, 10);
    
    gtk_box_pack_start(GTK_BOX(cores_box), label_cores, FALSE, FALSE, 0);
    gtk_box_pack_start(GTK_BOX(cores_box), field_cores, FALSE, FALSE, 0);
    gtk_box_pack_start(GTK_BOX(cores_box), num_cores, FALSE, FALSE, 0);
    
    //configurando box de processos
    label_processos = gtk_label_new("Quantidade de processos:");
    gtk_widget_set_valign(label_processos, GTK_ALIGN_START);
    gtk_widget_set_halign(label_processos, GTK_ALIGN_START);
    gtk_widget_set_margin_top(label_processos, 5);
    
    field_processos = gtk_entry_new();
    gtk_entry_set_max_length (GTK_ENTRY(field_processos), 0);
    gtk_widget_set_valign(field_processos, GTK_ALIGN_START);
    g_signal_connect(G_OBJECT(field_processos), "insert-text", G_CALLBACK(entry_number_filter), field_processos);
    
    gtk_box_pack_start(GTK_BOX(processos_box), label_processos, FALSE, FALSE, 0);
    gtk_box_pack_start(GTK_BOX(processos_box), field_processos, FALSE, FALSE, 0);
    
    //configurando box de cores em execução
    cores_running_label = gtk_label_new("Em execução:");
    gtk_widget_set_valign(cores_running_label, GTK_ALIGN_START);
    gtk_widget_set_halign(cores_running_label, GTK_ALIGN_START);
    gtk_widget_set_margin_start(cores_running_label, 5);
    gtk_widget_set_margin_start(cores_running_box, 5);
    
    gtk_box_pack_start(GTK_BOX(container_cores_box), cores_running_label, FALSE, FALSE, 0);
    gtk_box_pack_start(GTK_BOX(container_cores_box), cores_running_box, FALSE, FALSE, 0);
    
    //configurando box da fila de aptos
    container_processos_label = gtk_label_new("Aptos:");
    gtk_widget_set_valign(container_processos_label, GTK_ALIGN_START);
    gtk_widget_set_halign(container_processos_label, GTK_ALIGN_START);
    gtk_widget_set_margin_start(container_processos_label, 5);
    gtk_widget_set_margin_start(processos_fila_box, 5);
    
    gtk_box_pack_start(GTK_BOX(container_processos_box), container_processos_label, FALSE, FALSE, 0);
    gtk_box_pack_start(GTK_BOX(container_processos_box), processos_fila_box, FALSE, FALSE, 0);
    
    //configurando box de finalizados
    finalizados_label = gtk_label_new("Finalizados:");
    gtk_widget_set_valign(finalizados_label, GTK_ALIGN_START);
    gtk_widget_set_halign(finalizados_label, GTK_ALIGN_START);
    gtk_widget_set_margin_start(finalizados_label, 5);
    gtk_widget_set_margin_start(finalizados_box, 5);
    
    gtk_box_pack_start(GTK_BOX(container_finalizados_box), finalizados_label, FALSE, FALSE, 0);
    gtk_box_pack_start(GTK_BOX(container_finalizados_box), finalizados_box, FALSE, FALSE, 0);
    
    //configurando botões
    button_iniciar = gtk_button_new_with_label("Iniciar");
    button_add_dinamic = gtk_button_new_with_label("Adicionar Processo");
    p_ButtonParams->field_processos = field_processos;
    p_ButtonParams->field_cores = field_cores;
    p_ButtonParams->button = button_iniciar;
    p_ButtonParams->button_add = button_add_dinamic;
    p_ButtonParams->box_cores = cores_running_box;
    p_ButtonParams->box_processos = processos_fila_box;
    p_ButtonParams->box_finalizados = finalizados_box;
    gtk_widget_set_margin_top(button_iniciar, 26);
    g_signal_connect(G_OBJECT(button_iniciar), "clicked", G_CALLBACK(button_clicked), p_ButtonParams);
    
    gtk_widget_set_sensitive(button_add_dinamic, FALSE);
    gtk_widget_set_margin_start(button_add_dinamic, 5);
    g_signal_connect(G_OBJECT(button_add_dinamic), "clicked", G_CALLBACK(button_clicked_add), p_ButtonParams);
    
    gtk_box_pack_start(GTK_BOX(cores_box), button_add_dinamic, FALSE, FALSE, 0);
    gtk_box_pack_start(GTK_BOX(button_box), button_iniciar, FALSE, FALSE, 0);
    
    //adicionando as boxes
    gtk_box_pack_start(GTK_BOX(init_box), cores_box, FALSE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(init_box), processos_box, FALSE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(init_box), button_box, FALSE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(main_box), init_box, FALSE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(main_box), container_cores_box, FALSE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(main_box), container_processos_box, FALSE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(main_box), container_finalizados_box, FALSE, TRUE, 0);
    gtk_container_add(GTK_CONTAINER(scrolled_window), main_box);
    gtk_container_add(GTK_CONTAINER(frame), scrolled_window);
    
    //des-setando os valores
    g_value_unset(&editable_value);
    
    //criando tab
    label_sjf = gtk_label_new ("SJF");
    gtk_notebook_append_page (GTK_NOTEBOOK (notebook), frame, label_sjf);
}