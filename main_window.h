#ifndef __MAIN_WINDOW__
#define __MAIN_WINDOW__

typedef struct _GtkWidget GtkWidget;
typedef void* gpointer;

GtkWidget* getWindow();

void destroy(GtkWidget *widget, gpointer data);

void init_main_window();

#endif