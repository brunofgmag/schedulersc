#ifndef __PROCESSO_SJF__
#define __PROCESSO_SJF__

typedef struct processo_sjf processo_sjf;
typedef struct _GtkWidget GtkWidget;
typedef struct _GtkTextBuffer GtkTextBuffer;
typedef char gchar;

struct processo_sjf 
{
    unsigned short idProcesso;
    unsigned short tempoExecucao;
    unsigned short tempoRestante;
    unsigned short estadoProcesso;
    unsigned short tipo_processo;
    unsigned short is_last;
    GtkWidget* text_area;
    GtkWidget* processos_box;
    GtkWidget* finalizados_box;
    GtkTextBuffer* buffer;
    gchar* text;
    processo_sjf* next;
    
    void (*run)(processo_sjf *processo);
};

processo_sjf* criarProcesso(int idProcesso, int tipo_processo, GtkWidget* box, GtkWidget* fin_box, unsigned short is_last);

void sort(void* box);

void run(processo_sjf *processo);

int random_number(int min_num, int max_num, int idProcesso);

#endif