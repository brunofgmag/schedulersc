#include "cpu_sjf.h"
#include "core_sjf.h"
#include "escalonador_sjf.h"
#include <stdio.h>
#include <gtk/gtk.h>

void run_cpu(int numCores, escalonador_sjf* escalonador, GtkWidget* cores_box)
{
    int i;
    
    for(i = 0; i < numCores; i++)
    {
        criarCore(i, escalonador, cores_box);
        printf("Criando core %d\n", i);
    }
}